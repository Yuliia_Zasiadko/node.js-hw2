const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');
const {AuthorizationError} = require('../utils/errors');
const {DataError} = require('../utils/errors');

// const path = require('path');
// const envPath = path.resolve('./.env');
// console.log("🚀 ~ file: authService.js ~ line 10 ~ envPath", envPath)
// require('dotenv').config({path: path.resolve('./.env')});
require('dotenv').config({path: './.env'});
const SECRET = process.env.SECRET;
// console.log("🚀 ~ file: authService.js ~ line 10 ~ SECRET", SECRET);
// const fs = require('fs');
// console.log(fs.readFileSync(envPath, 'utf8'))

const registration = async ({username, password}) => {
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });
  try {
    await user.save();
  } catch (error) {
    throw new DataError('Username is already taken');
  }
};

const login = async ({username, password}) => {
  const user = await User.findOne({username});

  if (!user) {
    throw new AuthorizationError('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new AuthorizationError('Invalid email or password');
  }

  const token = jwt.sign({
    _id: user._id,
    username: user.username,
  }, SECRET);

  res.cookie('token', token);
  return token;
};

module.exports ={
  registration,
  login,
};
