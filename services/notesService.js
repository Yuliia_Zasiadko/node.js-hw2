const {Note} = require('../models/noteModel');
require('dotenv').config({path: './.env'});
const LIMIT = process.env.LIMIT;

const getAllNotesByUserId = async (userId, offset = 0, limit = LIMIT) => {
  const allNotes = await Note.find({userId}, {__v: 0})
      .skip(+offset)
      .limit(+limit);
  return {
    offset,
    limit,
    count: allNotes.length,
    notes: allNotes,
  };
};

const addNoteToUser = async (userId, text) => {
  const note = new Note({text, userId});
  await note.save();
};

const getNoteById = async (noteId, userId) => {
  return await Note.findOne({_id: noteId, userId});
};

const deleteNoteById = async (noteId, userId) => {
  await Note.findOneAndRemove({_id: noteId, userId});
};

const checkUncheckNoteById = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId});
  await note.updateOne({$set: {completed: !note.completed}});
};

const updateNoteById = async (noteId, userId, noteText) => {
  await Note.findOneAndUpdate({_id: noteId, userId}, {$set: {text: noteText}});
};

module.exports = {
  getAllNotesByUserId,
  addNoteToUser,
  getNoteById,
  deleteNoteById,
  checkUncheckNoteById,
  updateNoteById,
};
