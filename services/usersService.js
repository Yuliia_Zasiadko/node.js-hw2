const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');
const {DataError} = require('../utils/errors');

const getUserById = async (_id) => {
  console.log({_id});
  const user = await User.findOne({_id}, {password: 0, __v: 0});
  // const user = await User.find({_id}, {_id: 1, username: 1, createdDate: 1});
  // const {username, createdDate} = await User.find({_id});
  return user;
  // console.log("🚀 ~ file: usersService.js ~ line 11 ~ getUserById ~ ", {
  //   _id,
  //   username,
  //   createdDate
  // });
//   return {
//     _id,
//     username,
//     createdDate,
//   };
};

const deleteUserById = async (_id) => {
  await User.remove({_id});
};

const changeUserPasswordById = async (_id, oldPassword, newPassword) => {
  const user = await User.findOne({_id});
  console.log(oldPassword, user.password);
  if (!(await bcrypt.compare(oldPassword, user.password))) {
  // if (user.password !== oldPassword) {
    throw new DataError('Wrong password');
  }

  await User.updateOne({_id}, {$set:
    {password: await bcrypt.hash(newPassword, 10)},
  });
};

module.exports = {
  getUserById,
  deleteUserById,
  changeUserPasswordById,
};
